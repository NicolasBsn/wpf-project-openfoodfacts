using WPF_Project_OpenFoodFacts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Collections.ObjectModel;

namespace WPF_Project_OpenFoodFacts.Tests
{
    [TestClass()]
    public class UnitTest1
    {
        [TestMethod()]
        public void TypeOfBarcode()
        {
            VM vm = new VM();

            Assert.IsInstanceOfType(vm.Barcode, typeof(String));

        }


        [TestMethod()]
        public void TypeOfProducts()
        {
            VM vm = new VM();

            Assert.IsInstanceOfType(vm.Products, typeof(ObservableCollection<Product>));
            
        }
    }
}
