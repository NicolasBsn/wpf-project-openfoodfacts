﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_Project_OpenFoodFacts
{
    public class Product
    {
        public string product_name { get; set; }
        public string image_thumb_url { get; set; }
        public string brands { get; set; }
        public string nutrition_grade_fr { get; set; }
       
        public Nutriments nutriments { get; set; }
        public NutrientLevels nutrient_levels { get; set; }

        public string ingredients { get; set; }
        public string code { get; set; }
    }

    public class NutrientLevels
    {
        public string sugars { get; set; }
        public string salt { get; set; }
        public string fat { get; set; }
        public string saturated_fat { get; set; }
    }
    public class Nutriments
    {
        public string salt_value { get; set; }
        public string sodium_unit { get; set; }
        public string energy_value { get; set; }
        public string nutrition_score_fr { get; set; }
        public string carbohydrates_value { get; set; }
        public string saturated_fat_serving { get; set; }
        public string fat_serving { get; set; }
        public string fat_value { get; set; }
        public string proteins_unit { get; set; }
        public string sodium_100g { get; set; }
        public string saturated_fat_unit { get; set; }
        public string energy_serving { get; set; }
        public string sodium_value { get; set; }
        public string proteins_serving { get; set; }
        public string nutrition_score_uk { get; set; }
        public string saturated_fat { get; set; }
        public string proteins_value { get; set; }
        public string sodium_serving { get; set; }
        public string salt_serving { get; set; }
        public string sugars_serving { get; set; }
        public string fat_unit { get; set; }
        public string saturated_fat_100g { get; set; }
        public string sodium { get; set; }
        public string nutrition_score_fr_100g { get; set; }
        public string sugars_unit { get; set; }
        public string proteins { get; set; }
        public string sugars_100g { get; set; }
        public string proteins_100g { get; set; }
        public string carbohydrates { get; set; }
        public string saturated_fat_value { get; set; }
        public string carbohydrates_100g { get; set; }
        public string energy_100g { get; set; }
        public string fat_100g { get; set; }
        public string energy { get; set; }
        public string carbohydrates_unit { get; set; }
        public string salt { get; set; }
        public string fat { get; set; }
        public string salt_unit { get; set; }
        public string carbohydrates_serving { get; set; }
        public string sugars_value { get; set; }
        public string energy_unit { get; set; }
        public string nutrition_score_uk_100g { get; set; }
        public string sugars { get; set; }
        public string salt_100g { get; set; }

    }
    public class Ingredient
    {
        public string text { get; set; }

    }
}
