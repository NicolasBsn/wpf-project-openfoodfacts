﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Newtonsoft.Json.Linq;

namespace WPF_Project_OpenFoodFacts
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public VM vm;

        public MainWindow()
        {
            InitializeComponent();
            vm = new VM();
            this.DataContext = vm;
        }

        private void get_product(object sender, RoutedEventArgs e)
        {
            vm.get_product_from_VM();
        }

        private void get_products(object sender, RoutedEventArgs e)
        {
            vm.get_products_from_VM();
        }
    }
}
