﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace WPF_Project_OpenFoodFacts
{
    public class VM : INotifyPropertyChanged
    {



        ObservableCollection<Product> products;

        public VM()
        {
            Products = new ObservableCollection<Product>();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string barcode = "";
        public string Barcode
        {
            get { return this.barcode; }
            set
            {
                this.barcode = value;
                OnPropertyChanged("barcode");
            }
        }

        private string product = "";
        public string Product
        {
            get { return this.product; }
            set
            {
                this.product = value;
                OnPropertyChanged("product");
            }
        }
        public ObservableCollection<Product> Products
        {
            get
            {
                return products;
            }
            set
            {
                products = value;
                OnPropertyChanged("products");
            }
        }
        public void get_products_from_VM()
        {
            products.Clear();
            string json = get_products_from_api(this.product);
            Products = VM.get_products_deserialize(json);

        }
        public void get_product_from_VM()
        {
            products.Clear();
            string json = get_product_from_api(this.barcode, 1);
            var jObject = JObject.Parse(json);
            JToken jtoken = jObject["product"];
            Product product = VM.get_product_deserialize(jtoken);
            products.Add(product);
        }

    
        public static string get_product_from_api(string barcode, int page)
        {

            string res = "";
            string URL = "https://fr.openfoodfacts.org/api/v0/product/" + barcode;
            if (page >= 2)
            {
                URL = URL + "/" + page;
            }
            URL = URL + ".json";

            WebRequest webRequest;
            webRequest = WebRequest.Create(URL);
            webRequest.ContentType = "application/json; charset=UTF-8";

            try
            {
                HttpWebResponse response = ((HttpWebResponse)webRequest.GetResponse());
                {
                    StreamReader readFromRequest = new StreamReader(response.GetResponseStream());
                    res = readFromRequest.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            return res;
        }

        public static string get_products_from_api(string product)
        {

            string res = "";
            string URL = "https://world.openfoodfacts.org/cgi/search.pl?search_terms=" + product + "&search_simple=1&action=process&json=1";

            WebRequest webRequest;
            webRequest = WebRequest.Create(URL);
            webRequest.ContentType = "application/json; charset=UTF-8";

            try
            {
                HttpWebResponse response = ((HttpWebResponse)webRequest.GetResponse());
                {
                    StreamReader readFromRequest = new StreamReader(response.GetResponseStream());
                    res = readFromRequest.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            return res;
        }

        public static Product get_product_deserialize(JToken jtoken)
        {
            Product product = new Product();
            product.product_name = jtoken["product_name"].ToString();
            Nutriments result = JsonConvert.DeserializeObject<Nutriments>(jtoken["nutriments"].ToString());
            product.nutriments = result;
            product.nutrition_grade_fr = jtoken["nutrition_grade_fr"].ToString();
            product.nutrient_levels = JsonConvert.DeserializeObject<NutrientLevels>(jtoken["nutrient_levels"].ToString());
            List<Ingredient> ingredients = JsonConvert.DeserializeObject<List<Ingredient>>(jtoken["ingredients"].ToString());
            product.ingredients = "";
            ingredients.ForEach(item => product.ingredients += item.text +", ");
            if (jtoken["image_thumb_url"] != null)
            {
                product.image_thumb_url = jtoken["image_thumb_url"].ToString();
            }
            else
            {
                product.image_thumb_url = "";
            }
            product.brands = jtoken["brands"].ToString();
            product.code = jtoken["code"].ToString();
            return product;
        }

        public static ObservableCollection<Product> get_products_deserialize(string json)
        {
            var jObject = JObject.Parse(json);
            JToken jtokens = jObject["products"];
            ObservableCollection<Product> products_collection = new ObservableCollection<Product>();
            foreach (JToken product_by_token in jtokens)
            {
                Product product_deserialize = get_product_deserialize(product_by_token);
                products_collection.Add(product_deserialize);
            }
            return products_collection;
        }
    }
}
