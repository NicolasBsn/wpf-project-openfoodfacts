# WPF Project OpenFoodFacts

## Contexte

### Choix de la base de données
Ce projet a été réalisé pour le cours de C# ING4 SI. Nous avons choisi d’utiliser OpenFoodFacts pour notre projet : la base de données libre sur les produits alimentaires commercialisés dans le monde entier. 

### Collaborateurs 
* Nicolas BOUSSENINA - ING4 SI TD1
* Olivia DALMASSO - ING4 SI TD1
* Sebastien YE - ING4 SI TD1

### Fonctionnalités

Ce projet possède deux options principales : 
- chercher un objet à partir de son code barre (search with barcode)
- chercher un objet à partir de son nom de produit (search with product name)

### Technique
- Le projet est développé selon le modèle MVC avec WPF en C#
- Les données sont récupérées via l'API Open food facts
- Des tests unitaires sont aussi présents dans le projet dans un but pédagogique

### Informations des produits
Dans les deux cas précédents, nous pouvons obtenir les informations suivantes sur les produits :
* les ingrédients 
* le code barre 
* les niveaux nutritionnels
* le Nutriscore (rajouté pour compléter le projet)

### Vidéo démo du projet : https://drive.google.com/open?id=1FU0H8YmLOrgudNNTHqw04K-_CIH03kfz

### Front
Le front dispose des caractéristiques suivantes : 
* Titre animé changeant de couleur
* Fond d’écran nutritif
* Trigger - changement de couleur : lors du passage de la souris sur les boutons
* Police, texte, taille revus et adaptés
* Liste de produits

### Améliorations
Plusieurs améliorations son envisageables :
* Ajouter un champ de recherche avec la marque (Coca-cola, Pepsi etc)
* Ajouter un champ de recherche avec la catégorie du produit (Fruits, Légumes etc)
* Effectuer des tests unitaires plus précis sur ce que renvoie l'API OpenFoodFacts afin de s'assurer que les modèles retournés sont toujours les mêmes au fil des versions de l'API

### Documentation 
 
*Front : [https://www.tutorialspoint.com/wpf/)




